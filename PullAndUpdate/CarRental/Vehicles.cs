﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace CarRental
{
    public partial class Vehicles : Form
    {
        SqlCeConnection mySqlConnection;
        private string ID;
        public Vehicles()
        {
            InitializeComponent();
            populateListBox();
        }


        public void populateListBox()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");

            String selcmd = "SELECT VehicleID, VehicleName, VehicleBrand, VehicleType, RentalFee, Available, BHP, Size, Modifications, Seats FROM tblVehicles ORDER BY VehicleID";

            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                lbxvehicles.Items.Clear();

                while (mySqlDataReader.Read())
                {
                    lbxvehicles.Items.Add(mySqlDataReader["VehicleID"] + " " +
                         mySqlDataReader["VehicleName"] + " " + mySqlDataReader["VehicleBrand"] + " " +
                         mySqlDataReader["VehicleType"] + " £" + mySqlDataReader["RentalFee"] + " " +
                         mySqlDataReader["Available"] + " " + mySqlDataReader["BHP"] + " " + 
                         mySqlDataReader["Size"] + " " + mySqlDataReader["Modifications"] + " " + mySqlDataReader["Seats"]);
                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void populatetext()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");


            String selcmd = "SELECT VehicleID, VehicleName, VehicleBrand, VehicleType, RentalFee, Available, BHP, Size, Modifications, Seats FROM tblVehicles WHERE VehicleID = "+ txtID.Text +"";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();
                SqlCeDataReader rdr = mySqlCommand.ExecuteReader();
                while (rdr.Read())

                {
                    string s1 = rdr["VehicleName"].ToString();
                    txtname.Text = s1;
                    string s2 = rdr["VehicleBrand"].ToString();
                    txtbrand.Text = s2;
                    string s3 = rdr["VehicleType"].ToString();
                    txttype.Text = s3;
                    string s4 = rdr["RentalFee"].ToString();
                    txtfee.Text = s4;
                    string s5 = rdr["Available"].ToString();
                    txtavailable.Text = s5;
                    string s6 = rdr["BHP"].ToString();
                    txtbhp.Text = s6;
                    string s7 = rdr["Size"].ToString();
                    txtsize.Text = s7;
                    string s8 = rdr["Modifications"].ToString();
                    txtmod.Text = s8;
                    string s9 = rdr["Seats"].ToString();
                    txtseats.Text = s9;
                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void insertRecord(String ID, String name, String brand, String type, String fee, String available, String bhp, String size, String mod, String seats, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@ID", ID);
                cmdInsert.Parameters.AddWithValue("@name", name);
                cmdInsert.Parameters.AddWithValue("@brand", brand);
                cmdInsert.Parameters.AddWithValue("@type", type);
                cmdInsert.Parameters.AddWithValue("@fee", fee);
                cmdInsert.Parameters.AddWithValue("@available", available);
                cmdInsert.Parameters.AddWithValue("@bhp", bhp);
                cmdInsert.Parameters.AddWithValue("@size", size);
                cmdInsert.Parameters.AddWithValue("@mod", mod);
                cmdInsert.Parameters.AddWithValue("@seats", seats);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void deleteRecord(String ID, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@ID", ID);
                cmdInsert.ExecuteNonQuery();

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public void cleartxtBoxes()       
        {
            txtID.Text = txtname.Text = txtbrand.Text = txttype.Text = txtfee.Text =  txtbhp.Text = txtsize.Text = txtmod.Text = txtseats.Text = "";
            txtavailable.Text = "True/False";

        }


        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtID.Text) ||
                string.IsNullOrEmpty(txtname.Text) ||
                string.IsNullOrEmpty(txtbrand.Text) ||
                string.IsNullOrEmpty(txttype.Text) ||
                string.IsNullOrEmpty(txtavailable.Text) ||
                string.IsNullOrEmpty(txtfee.Text)) 
                //Not all fields, only important fields
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        
        }

        public bool checkInputsID()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtID.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        private void Vehicles_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {

                String commandString = "INSERT INTO tblVehicles(VehicleID, VehicleName, VehicleBrand, VehicleType, RentalFee, Available, BHP, Size, Modifications, Seats) VALUES (@ID, @name, @brand, @type, @fee, @available, @bhp, @size, @mod, @seats)";

                insertRecord(txtID.Text, txtname.Text, txtbrand.Text, txttype.Text, txtfee.Text, txtavailable.Text, txtbhp.Text, txtsize.Text, txtmod.Text, txtseats.Text, commandString);
                populateListBox();
                cleartxtBoxes();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (checkInputsID())
            {

                String commandString = "DELETE FROM tblVehicles WHERE VehicleID = @ID";

                deleteRecord(txtID.Text, commandString);
                populateListBox();
                cleartxtBoxes();
            }
        }

        private void btnpull_Click(object sender, EventArgs e)
        {
            if (checkInputsID())
            {
                String commandString = "SELECT VehicleID, VehicleName, VehicleBrand, VehicleType, RentalFee, Available, BHP, Size, Modifications, Seats FROM tblVehicles WHERE VehicleID =  " + txtID.Text + "";
                //Executes SQL statement with specified parameters
                //deleteRecord(txtID.Text, commandString);

                populatetext();
                //Use this button to add values into the text fields 
            }
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {

                String commandString = "UPDATE tblVehicles SET  VehicleName=@name, VehicleBrand=@brand, VehicleType=@type, RentalFee=@fee, Available=@available, BHP=@bhp, Size=@size, Modifications=@mod, Seats=@seats WHERE VehicleID = @ID";
                //Executes SQL statement with specified parameters
                insertRecord(txtID.Text, txtname.Text, txtbrand.Text, txttype.Text, txtfee.Text, txtavailable.Text, txtbhp.Text, txtsize.Text, txtmod.Text, txtseats.Text, commandString);
                populateListBox();
                cleartxtBoxes();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            cleartxtBoxes();
        }
    }
}
