﻿namespace CarRental
{
    partial class Vehicles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label lblSize;
            this.btnDelete = new System.Windows.Forms.Button();
            this.lbxvehicles = new System.Windows.Forms.ListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblfee = new System.Windows.Forms.Label();
            this.lbltype = new System.Windows.Forms.Label();
            this.lblbrand = new System.Windows.Forms.Label();
            this.lblname = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.txtfee = new System.Windows.Forms.TextBox();
            this.txttype = new System.Windows.Forms.TextBox();
            this.txtbrand = new System.Windows.Forms.TextBox();
            this.txtname = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.lblAvailable = new System.Windows.Forms.Label();
            this.lblbhp = new System.Windows.Forms.Label();
            this.lblMods = new System.Windows.Forms.Label();
            this.lblSeats = new System.Windows.Forms.Label();
            this.txtavailable = new System.Windows.Forms.TextBox();
            this.txtbhp = new System.Windows.Forms.TextBox();
            this.txtsize = new System.Windows.Forms.TextBox();
            this.txtmod = new System.Windows.Forms.TextBox();
            this.txtseats = new System.Windows.Forms.TextBox();
            this.btnupdate = new System.Windows.Forms.Button();
            this.btnpull = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            lblSize = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblSize
            // 
            lblSize.AutoSize = true;
            lblSize.Location = new System.Drawing.Point(41, 258);
            lblSize.Name = "lblSize";
            lblSize.Size = new System.Drawing.Size(27, 13);
            lblSize.TabIndex = 28;
            lblSize.Text = "Size";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(44, 362);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 25;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lbxvehicles
            // 
            this.lbxvehicles.FormattingEnabled = true;
            this.lbxvehicles.Location = new System.Drawing.Point(246, 24);
            this.lbxvehicles.Name = "lbxvehicles";
            this.lbxvehicles.Size = new System.Drawing.Size(480, 316);
            this.lbxvehicles.TabIndex = 24;
            //this.lbxvehicles.SelectedIndexChanged += new System.EventHandler(this.lbxvehicles_SelectedIndexChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(125, 362);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 23);
            this.btnAdd.TabIndex = 23;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblfee
            // 
            this.lblfee.AutoSize = true;
            this.lblfee.Location = new System.Drawing.Point(41, 164);
            this.lblfee.Name = "lblfee";
            this.lblfee.Size = new System.Drawing.Size(59, 13);
            this.lblfee.TabIndex = 22;
            this.lblfee.Text = "Rental Fee";
            // 
            // lbltype
            // 
            this.lbltype.AutoSize = true;
            this.lbltype.Location = new System.Drawing.Point(41, 128);
            this.lbltype.Name = "lbltype";
            this.lbltype.Size = new System.Drawing.Size(31, 13);
            this.lbltype.TabIndex = 21;
            this.lbltype.Text = "Type";
            // 
            // lblbrand
            // 
            this.lblbrand.AutoSize = true;
            this.lblbrand.Location = new System.Drawing.Point(41, 98);
            this.lblbrand.Name = "lblbrand";
            this.lblbrand.Size = new System.Drawing.Size(35, 13);
            this.lblbrand.TabIndex = 20;
            this.lblbrand.Text = "Brand";
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.Location = new System.Drawing.Point(41, 63);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(35, 13);
            this.lblname.TabIndex = 19;
            this.lblname.Text = "Name";
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(41, 27);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(56, 13);
            this.lblID.TabIndex = 18;
            this.lblID.Text = "Vehicle ID";
            // 
            // txtfee
            // 
            this.txtfee.Location = new System.Drawing.Point(120, 161);
            this.txtfee.Name = "txtfee";
            this.txtfee.Size = new System.Drawing.Size(100, 20);
            this.txtfee.TabIndex = 17;
            // 
            // txttype
            // 
            this.txttype.Location = new System.Drawing.Point(120, 125);
            this.txttype.Name = "txttype";
            this.txttype.Size = new System.Drawing.Size(100, 20);
            this.txttype.TabIndex = 16;
            // 
            // txtbrand
            // 
            this.txtbrand.Location = new System.Drawing.Point(120, 95);
            this.txtbrand.Name = "txtbrand";
            this.txtbrand.Size = new System.Drawing.Size(100, 20);
            this.txtbrand.TabIndex = 15;
            // 
            // txtname
            // 
            this.txtname.Location = new System.Drawing.Point(120, 60);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(100, 20);
            this.txtname.TabIndex = 14;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(120, 24);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 13;
            // 
            // lblAvailable
            // 
            this.lblAvailable.AutoSize = true;
            this.lblAvailable.Location = new System.Drawing.Point(41, 197);
            this.lblAvailable.Name = "lblAvailable";
            this.lblAvailable.Size = new System.Drawing.Size(50, 13);
            this.lblAvailable.TabIndex = 26;
            this.lblAvailable.Text = "Available";
            // 
            // lblbhp
            // 
            this.lblbhp.AutoSize = true;
            this.lblbhp.Location = new System.Drawing.Point(41, 229);
            this.lblbhp.Name = "lblbhp";
            this.lblbhp.Size = new System.Drawing.Size(29, 13);
            this.lblbhp.TabIndex = 27;
            this.lblbhp.Text = "BHP";
            // 
            // lblMods
            // 
            this.lblMods.AutoSize = true;
            this.lblMods.Location = new System.Drawing.Point(41, 288);
            this.lblMods.Name = "lblMods";
            this.lblMods.Size = new System.Drawing.Size(69, 13);
            this.lblMods.TabIndex = 29;
            this.lblMods.Text = "Modifications";
            // 
            // lblSeats
            // 
            this.lblSeats.AutoSize = true;
            this.lblSeats.Location = new System.Drawing.Point(41, 318);
            this.lblSeats.Name = "lblSeats";
            this.lblSeats.Size = new System.Drawing.Size(34, 13);
            this.lblSeats.TabIndex = 30;
            this.lblSeats.Text = "Seats";
            // 
            // txtavailable
            // 
            this.txtavailable.Location = new System.Drawing.Point(120, 197);
            this.txtavailable.Name = "txtavailable";
            this.txtavailable.Size = new System.Drawing.Size(100, 20);
            this.txtavailable.TabIndex = 18;
            this.txtavailable.Text = "True/False";
            // 
            // txtbhp
            // 
            this.txtbhp.Location = new System.Drawing.Point(120, 229);
            this.txtbhp.Name = "txtbhp";
            this.txtbhp.Size = new System.Drawing.Size(100, 20);
            this.txtbhp.TabIndex = 19;
            // 
            // txtsize
            // 
            this.txtsize.Location = new System.Drawing.Point(120, 258);
            this.txtsize.Name = "txtsize";
            this.txtsize.Size = new System.Drawing.Size(100, 20);
            this.txtsize.TabIndex = 20;
            // 
            // txtmod
            // 
            this.txtmod.Location = new System.Drawing.Point(120, 288);
            this.txtmod.Name = "txtmod";
            this.txtmod.Size = new System.Drawing.Size(100, 20);
            this.txtmod.TabIndex = 21;
            // 
            // txtseats
            // 
            this.txtseats.Location = new System.Drawing.Point(120, 318);
            this.txtseats.Name = "txtseats";
            this.txtseats.Size = new System.Drawing.Size(100, 20);
            this.txtseats.TabIndex = 22;
            // 
            // btnupdate
            // 
            this.btnupdate.Location = new System.Drawing.Point(312, 362);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(75, 23);
            this.btnupdate.TabIndex = 37;
            this.btnupdate.Text = "Update";
            this.btnupdate.UseVisualStyleBackColor = true;
            this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
            // 
            // btnpull
            // 
            this.btnpull.Location = new System.Drawing.Point(231, 362);
            this.btnpull.Name = "btnpull";
            this.btnpull.Size = new System.Drawing.Size(75, 23);
            this.btnpull.TabIndex = 38;
            this.btnpull.Text = "Pull";
            this.btnpull.UseVisualStyleBackColor = true;
            this.btnpull.Click += new System.EventHandler(this.btnpull_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(393, 362);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 39;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // Vehicles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 409);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnpull);
            this.Controls.Add(this.btnupdate);
            this.Controls.Add(this.txtseats);
            this.Controls.Add(this.txtmod);
            this.Controls.Add(this.txtsize);
            this.Controls.Add(this.txtbhp);
            this.Controls.Add(this.txtavailable);
            this.Controls.Add(this.lblSeats);
            this.Controls.Add(this.lblMods);
            this.Controls.Add(lblSize);
            this.Controls.Add(this.lblbhp);
            this.Controls.Add(this.lblAvailable);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lbxvehicles);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblfee);
            this.Controls.Add(this.lbltype);
            this.Controls.Add(this.lblbrand);
            this.Controls.Add(this.lblname);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.txtfee);
            this.Controls.Add(this.txttype);
            this.Controls.Add(this.txtbrand);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.txtID);
            this.Name = "Vehicles";
            this.Text = "Vehicles";
            this.Load += new System.EventHandler(this.Vehicles_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.ListBox lbxvehicles;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lblfee;
        private System.Windows.Forms.Label lbltype;
        private System.Windows.Forms.Label lblbrand;
        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TextBox txtfee;
        private System.Windows.Forms.TextBox txttype;
        private System.Windows.Forms.TextBox txtbrand;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label lblAvailable;
        private System.Windows.Forms.Label lblbhp;
        private System.Windows.Forms.Label lblMods;
        private System.Windows.Forms.Label lblSeats;
        private System.Windows.Forms.TextBox txtavailable;
        private System.Windows.Forms.TextBox txtbhp;
        private System.Windows.Forms.TextBox txtsize;
        private System.Windows.Forms.TextBox txtmod;
        private System.Windows.Forms.TextBox txtseats;
        private System.Windows.Forms.Button btnupdate;
        private System.Windows.Forms.Button btnpull;
        private System.Windows.Forms.Button btnClear;
    }
}