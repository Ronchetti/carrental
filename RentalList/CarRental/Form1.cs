﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace CarRental
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnAccounts_Click(object sender, EventArgs e)
        {
            frmRegister Register = new frmRegister();
            Register.Show();
        }

        private void btnVehicles_Click(object sender, EventArgs e)
        {
            Vehicles Vehicles = new Vehicles();
            Vehicles.Show();
        }

        private void btnbrowse_Click(object sender, EventArgs e)
        {
            Browser browser = new Browser();
            browser.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String s = "1";
            Rentals rental = new Rentals(s);
            rental.Show();
        }

        private void btnrentlist_Click(object sender, EventArgs e)
        {
            RentList rental = new RentList();
            rental.Show();
        }
    }
}
