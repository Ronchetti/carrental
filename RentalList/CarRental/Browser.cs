﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace CarRental
{
    public partial class Browser : Form
    {
        SqlCeConnection mySqlConnection;
        private string ID;
        
        public Browser()
        {
            InitializeComponent();
            populatetype();
            String cmd = "SELECT VehicleID, VehicleName, VehicleBrand, VehicleType, RentalFee, Available, BHP, Size, Modifications, Seats FROM tblVehicles ORDER BY VehicleID";
            populatestock(cmd);
        }

        public void populatetype()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            String selcmd = "SELECT DISTINCT VehicleType FROM tblVehicles  WHERE Available = 'True' ORDER BY VehicleType";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                lbxtype.Items.Clear();
                while (mySqlDataReader.Read())
                {
                    lbxtype.Items.Add(mySqlDataReader["VehicleType"]);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void populateseats(string command)
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            SqlCeCommand mySqlCommand = new SqlCeCommand(command, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                lbxseats.Items.Clear();
                while (mySqlDataReader.Read())
                {
                    lbxseats.Items.Add(mySqlDataReader["Seats"]);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void populatebrand(string command)
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            SqlCeCommand mySqlCommand = new SqlCeCommand(command, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                lbxbrand.Items.Clear();
                while (mySqlDataReader.Read())
                {
                    lbxbrand.Items.Add(mySqlDataReader["VehicleBrand"]);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void populatestock(string selcmd)
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                lbxstock.Items.Clear();
                while (mySqlDataReader.Read())
                {
                    lbxstock.Items.Add(mySqlDataReader["VehicleID"] + "-" +
                         mySqlDataReader["VehicleName"] + " " + mySqlDataReader["VehicleBrand"] + " " +
                         mySqlDataReader["VehicleType"] + " £" + mySqlDataReader["RentalFee"] + " " +
                         mySqlDataReader["Available"] + " " + mySqlDataReader["BHP"] + " " +
                         mySqlDataReader["Size"] + " " + mySqlDataReader["Modifications"] + " " + mySqlDataReader["Seats"]);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Browser_Load(object sender, EventArgs e)
        {

        }

        private void lblseats_Click(object sender, EventArgs e)
        {

        }

        private void lbxtype_SelectedIndexChanged(object sender, EventArgs e)
        {
           // SQL statements for displaying contextual data         
            String commandString = "SELECT DISTINCT Seats FROM tblVehicles WHERE VehicleType = '"+ lbxtype.SelectedItem.ToString() +"' AND Available = 'True'";
            String commandstock = "SELECT VehicleID, VehicleName, VehicleBrand, VehicleType, RentalFee, Available, BHP, Size, Modifications, Seats FROM tblVehicles WHERE VehicleType =  '" + lbxtype.SelectedItem.ToString() +"' AND Available = 'True'";
            //Sending SQL statement to be processed
            populateseats(commandString);
            populatestock(commandstock);
            //Clearing unwanted data
            lbxbrand.Items.Clear();
        }

        private void lbxseats_SelectedIndexChanged(object sender, EventArgs e)
        {
            // SQL statements for displaying contextual data        
            String commandString = "SELECT DISTINCT VehicleBrand FROM tblVehicles WHERE (VehicleType = '" + lbxtype.SelectedItem.ToString() + "') AND (Available = 'True') AND (Seats = '"+ lbxseats.SelectedItem.ToString() +"')";
            String commandstock = "SELECT VehicleID, VehicleName, VehicleBrand, VehicleType, RentalFee, Available, BHP, Size, Modifications, Seats FROM tblVehicles WHERE (VehicleType = '" + lbxtype.SelectedItem.ToString() + "') AND (Available = 'True') AND (Seats = '" + lbxseats.SelectedItem.ToString() + "')";
            //Sending SQL statement to be processed
            populatebrand(commandString);
            populatestock(commandstock);
        }

        private void lbxbrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            //SQL Statement for master stock table
            String commandstock = "SELECT VehicleID, VehicleName, VehicleBrand, VehicleType, RentalFee, Available, BHP, Size, Modifications, Seats FROM tblVehicles WHERE (VehicleType = '" + lbxtype.SelectedItem.ToString() + "') AND (Available = 'True') AND (Seats = '" + lbxseats.SelectedItem.ToString() + "') AND (VehicleBrand = '" + lbxbrand.SelectedItem.ToString() + "')";
            //Sending SQL statement to be processed
            populatestock(commandstock);
        }

        private void lbxstock_SelectedIndexChanged(object sender, EventArgs e)
        {
            string input = lbxstock.GetItemText(lbxstock.SelectedItem);
           // string text = lbxtype.GetItemText(lbxtype.SelectedItem);
            int index = input.IndexOf("-");
            if (index > 0)
                input = input.Substring(0, index);
            Rentals rental = new Rentals(input);
            rental.Show();
        }
    }
}
