﻿namespace CarRental
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAccounts = new System.Windows.Forms.Button();
            this.btnVehicles = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnbrowse = new System.Windows.Forms.Button();
            this.btnrentlist = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAccounts
            // 
            this.btnAccounts.Location = new System.Drawing.Point(77, 23);
            this.btnAccounts.Name = "btnAccounts";
            this.btnAccounts.Size = new System.Drawing.Size(135, 38);
            this.btnAccounts.TabIndex = 0;
            this.btnAccounts.Text = "Manage Accounts";
            this.btnAccounts.UseVisualStyleBackColor = true;
            this.btnAccounts.Click += new System.EventHandler(this.btnAccounts_Click);
            // 
            // btnVehicles
            // 
            this.btnVehicles.Location = new System.Drawing.Point(77, 67);
            this.btnVehicles.Name = "btnVehicles";
            this.btnVehicles.Size = new System.Drawing.Size(135, 38);
            this.btnVehicles.TabIndex = 1;
            this.btnVehicles.Text = "Manage Vehicles";
            this.btnVehicles.UseVisualStyleBackColor = true;
            this.btnVehicles.Click += new System.EventHandler(this.btnVehicles_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(77, 111);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(135, 38);
            this.button1.TabIndex = 2;
            this.button1.Text = "Rentals";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnbrowse
            // 
            this.btnbrowse.Location = new System.Drawing.Point(77, 155);
            this.btnbrowse.Name = "btnbrowse";
            this.btnbrowse.Size = new System.Drawing.Size(135, 38);
            this.btnbrowse.TabIndex = 3;
            this.btnbrowse.Text = "Browse stock";
            this.btnbrowse.UseVisualStyleBackColor = true;
            this.btnbrowse.Click += new System.EventHandler(this.btnbrowse_Click);
            // 
            // btnrentlist
            // 
            this.btnrentlist.Location = new System.Drawing.Point(77, 199);
            this.btnrentlist.Name = "btnrentlist";
            this.btnrentlist.Size = new System.Drawing.Size(135, 38);
            this.btnrentlist.TabIndex = 4;
            this.btnrentlist.Text = "Rental list";
            this.btnrentlist.UseVisualStyleBackColor = true;
            this.btnrentlist.Click += new System.EventHandler(this.btnrentlist_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btnrentlist);
            this.Controls.Add(this.btnbrowse);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnVehicles);
            this.Controls.Add(this.btnAccounts);
            this.Name = "Form1";
            this.Text = "Main menu";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAccounts;
        private System.Windows.Forms.Button btnVehicles;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnbrowse;
        private System.Windows.Forms.Button btnrentlist;
    }
}

