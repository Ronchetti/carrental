﻿namespace CarRental
{
    partial class Browser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxtype = new System.Windows.Forms.ListBox();
            this.lbxstock = new System.Windows.Forms.ListBox();
            this.lbxseats = new System.Windows.Forms.ListBox();
            this.lbltype = new System.Windows.Forms.Label();
            this.lblseats = new System.Windows.Forms.Label();
            this.lblbrand = new System.Windows.Forms.Label();
            this.lbxbrand = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbxtype
            // 
            this.lbxtype.FormattingEnabled = true;
            this.lbxtype.Location = new System.Drawing.Point(12, 38);
            this.lbxtype.Name = "lbxtype";
            this.lbxtype.Size = new System.Drawing.Size(122, 56);
            this.lbxtype.TabIndex = 0;
            this.lbxtype.SelectedIndexChanged += new System.EventHandler(this.lbxtype_SelectedIndexChanged);
            // 
            // lbxstock
            // 
            this.lbxstock.FormattingEnabled = true;
            this.lbxstock.Location = new System.Drawing.Point(149, 12);
            this.lbxstock.Name = "lbxstock";
            this.lbxstock.Size = new System.Drawing.Size(359, 316);
            this.lbxstock.TabIndex = 1;
            this.lbxstock.SelectedIndexChanged += new System.EventHandler(this.lbxstock_SelectedIndexChanged);
            // 
            // lbxseats
            // 
            this.lbxseats.FormattingEnabled = true;
            this.lbxseats.Location = new System.Drawing.Point(12, 113);
            this.lbxseats.Name = "lbxseats";
            this.lbxseats.Size = new System.Drawing.Size(120, 95);
            this.lbxseats.TabIndex = 2;
            this.lbxseats.SelectedIndexChanged += new System.EventHandler(this.lbxseats_SelectedIndexChanged);
            // 
            // lbltype
            // 
            this.lbltype.AutoSize = true;
            this.lbltype.Location = new System.Drawing.Point(12, 22);
            this.lbltype.Name = "lbltype";
            this.lbltype.Size = new System.Drawing.Size(65, 13);
            this.lbltype.TabIndex = 3;
            this.lbltype.Text = "Vehicle type";
            // 
            // lblseats
            // 
            this.lblseats.AutoSize = true;
            this.lblseats.Location = new System.Drawing.Point(9, 97);
            this.lblseats.Name = "lblseats";
            this.lblseats.Size = new System.Drawing.Size(84, 13);
            this.lblseats.TabIndex = 4;
            this.lblseats.Text = "Number of seats";
            this.lblseats.Click += new System.EventHandler(this.lblseats_Click);
            // 
            // lblbrand
            // 
            this.lblbrand.AutoSize = true;
            this.lblbrand.Location = new System.Drawing.Point(15, 215);
            this.lblbrand.Name = "lblbrand";
            this.lblbrand.Size = new System.Drawing.Size(35, 13);
            this.lblbrand.TabIndex = 5;
            this.lblbrand.Text = "Brand";
            // 
            // lbxbrand
            // 
            this.lbxbrand.FormattingEnabled = true;
            this.lbxbrand.Location = new System.Drawing.Point(12, 232);
            this.lbxbrand.Name = "lbxbrand";
            this.lbxbrand.Size = new System.Drawing.Size(120, 95);
            this.lbxbrand.TabIndex = 6;
            this.lbxbrand.SelectedIndexChanged += new System.EventHandler(this.lbxbrand_SelectedIndexChanged);
            // 
            // Browser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 343);
            this.Controls.Add(this.lbxbrand);
            this.Controls.Add(this.lblbrand);
            this.Controls.Add(this.lblseats);
            this.Controls.Add(this.lbltype);
            this.Controls.Add(this.lbxseats);
            this.Controls.Add(this.lbxstock);
            this.Controls.Add(this.lbxtype);
            this.Name = "Browser";
            this.Text = "Browser";
            this.Load += new System.EventHandler(this.Browser_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbxtype;
        private System.Windows.Forms.ListBox lbxstock;
        private System.Windows.Forms.ListBox lbxseats;
        private System.Windows.Forms.Label lbltype;
        private System.Windows.Forms.Label lblseats;
        private System.Windows.Forms.Label lblbrand;
        private System.Windows.Forms.ListBox lbxbrand;
    }
}