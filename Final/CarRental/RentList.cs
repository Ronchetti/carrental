﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Data.SqlClient;

namespace CarRental
{
    public partial class RentList : Form
    {
        DataTable data = new DataTable();
        SqlCeConnection mySqlConnection;
        SqlCeDataAdapter dataAdapter;
        public RentList()
        {
            InitializeComponent();
            String selcmd = "SELECT * FROM tblRentalInvoice ORDER BY RentalID";
            Datagrod(selcmd);
        }

       private void Datagrod(string selcmd)
        {
           //Database connection
            mySqlConnection =
                   new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
           //New data table
            data =  new DataTable();

           //Creating a new data adapter 
            dataAdapter = new SqlCeDataAdapter(selcmd, mySqlConnection);

           //Populating the data table
            dataAdapter.Fill(data);
            dgvRent.DataSource = data;

        }
        private void RentList_Load(object sender, EventArgs e)
        {

        }

        private void tbtotalmin_ValueChanged(object sender, EventArgs e)
        {
            //Assigning an SQL statement to a string
            String selcmd = "SELECT * FROM tblRentalInvoice WHERE Total BETWEEN '" + tbtotalmin.Value + "' AND '" + tbtotalmax.Value + "' ORDER BY RentalID";
            //Passing the SQL statement 
            Datagrod(selcmd);
        }

        private void tbtotalmax_ValueChanged(object sender, EventArgs e)
        {
            //Assigning an SQL statement to a string
            String selcmd = "SELECT * FROM tblRentalInvoice WHERE Total BETWEEN '" + tbtotalmin.Value + "' AND '" + tbtotalmax.Value + "' ORDER BY RentalID";
            //Passing the SQL statement 
            Datagrod(selcmd);
        }
    }
}
