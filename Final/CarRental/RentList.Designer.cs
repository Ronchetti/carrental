﻿namespace CarRental
{
    partial class RentList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvRent = new System.Windows.Forms.DataGridView();
            this.tbtotalmin = new System.Windows.Forms.TrackBar();
            this.tbtotalmax = new System.Windows.Forms.TrackBar();
            this.lbltotal = new System.Windows.Forms.Label();
            this.lblmintotal = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbtotalmin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbtotalmax)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvRent
            // 
            this.dgvRent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRent.Location = new System.Drawing.Point(429, 12);
            this.dgvRent.Name = "dgvRent";
            this.dgvRent.Size = new System.Drawing.Size(814, 473);
            this.dgvRent.TabIndex = 0;
            // 
            // tbtotalmin
            // 
            this.tbtotalmin.BackColor = System.Drawing.SystemColors.Control;
            this.tbtotalmin.Cursor = System.Windows.Forms.Cursors.Default;
            this.tbtotalmin.Location = new System.Drawing.Point(82, 62);
            this.tbtotalmin.Maximum = 2000;
            this.tbtotalmin.Minimum = 1;
            this.tbtotalmin.Name = "tbtotalmin";
            this.tbtotalmin.Size = new System.Drawing.Size(268, 45);
            this.tbtotalmin.TabIndex = 1;
            this.tbtotalmin.Value = 1;
            this.tbtotalmin.ValueChanged += new System.EventHandler(this.tbtotalmin_ValueChanged);
            // 
            // tbtotalmax
            // 
            this.tbtotalmax.Location = new System.Drawing.Point(82, 113);
            this.tbtotalmax.Maximum = 2000;
            this.tbtotalmax.Minimum = 1;
            this.tbtotalmax.Name = "tbtotalmax";
            this.tbtotalmax.Size = new System.Drawing.Size(268, 45);
            this.tbtotalmax.TabIndex = 2;
            this.tbtotalmax.Value = 2000;
            this.tbtotalmax.ValueChanged += new System.EventHandler(this.tbtotalmax_ValueChanged);
            // 
            // lbltotal
            // 
            this.lbltotal.AutoSize = true;
            this.lbltotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltotal.Location = new System.Drawing.Point(78, 25);
            this.lbltotal.Name = "lbltotal";
            this.lbltotal.Size = new System.Drawing.Size(51, 24);
            this.lbltotal.TabIndex = 5;
            this.lbltotal.Text = "Total";
            // 
            // lblmintotal
            // 
            this.lblmintotal.AutoSize = true;
            this.lblmintotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmintotal.Location = new System.Drawing.Point(22, 92);
            this.lblmintotal.Name = "lblmintotal";
            this.lblmintotal.Size = new System.Drawing.Size(66, 30);
            this.lblmintotal.TabIndex = 6;
            this.lblmintotal.Text = "Minimum: \r\n1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(345, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 30);
            this.label1.TabIndex = 7;
            this.label1.Text = "Maximum: \r\n2000";
            // 
            // RentList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1255, 500);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblmintotal);
            this.Controls.Add(this.lbltotal);
            this.Controls.Add(this.tbtotalmax);
            this.Controls.Add(this.tbtotalmin);
            this.Controls.Add(this.dgvRent);
            this.Name = "RentList";
            this.Text = "RentList";
            this.Load += new System.EventHandler(this.RentList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbtotalmin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbtotalmax)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRent;
        private System.Windows.Forms.TrackBar tbtotalmin;
        private System.Windows.Forms.TrackBar tbtotalmax;
        private System.Windows.Forms.Label lbltotal;
        private System.Windows.Forms.Label lblmintotal;
        private System.Windows.Forms.Label label1;
    }
}