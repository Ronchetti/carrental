﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Drawing.Printing;
using System.Drawing.Imaging;
namespace CarRental
{
    public partial class Invoice : Form
    {
        SqlCeConnection mySqlConnection;
        private string ID;
        public Invoice()
        {
            InitializeComponent();
        }

        public void populatetext(string selcmd)
        {
            //Database location
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            //SQL statement for selecting items from a database
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader rdr = mySqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    //Populating the labels boxes with relevant information
                    string s1 = rdr["CustomerID"].ToString();
                    lblcust2.Text = s1;
                    string s2 = rdr["VehicleID"].ToString();
                    lblvid2.Text = s2;
                    string s3 = rdr["RentalDate"].ToString();
                    lblrented2.Text = s3;
                    string s4 = rdr["ReturnDate"].ToString();
                    lblreturned2.Text = s4;
                    string s5 = rdr["Fee"].ToString();
                    lblfee2.Text = s5;
                    string s6 = rdr["Total"].ToString();
                    lbltotal2.Text = s6;
                    string s7 = rdr["Damage"].ToString();
                    lbldamage2.Text = s7;
                }
            }
            catch (SqlCeException ex)
            {

                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void populatetext(string selcmd, int a)
        {
            //Database location
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            //SQL statement for selecting items from a database
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader rdr = mySqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    //Populating the text boxes with relevant information
                    string s1 = rdr["CustomerFirstName"].ToString();
                    lblname2.Text = s1;
                    string s2 = rdr["CustomerLastName"].ToString();
                    lbllast2.Text = s2;
            
                }
            }
            catch (SqlCeException ex)
            {

                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void populatetext(string selcmd, double a)
        {
            //Database location
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            //SQL statement for selecting items from a database
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader rdr = mySqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    //Populating the text boxes with relevant information
                    string s1 = rdr["VehicleName"].ToString();
                    lblvname2.Text = s1;
                    string s2 = rdr["VehicleBrand"].ToString();
                    lblbrand2.Text = s2;

                }
            }
            catch (SqlCeException ex)
            {

                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Invoice_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtrent_TextChanged(object sender, EventArgs e)
        {
            String selcmd = "SELECT * FROM tblRentalInvoice WHERE RentalID = '"+txtrent.Text+"'";
            populatetext(selcmd);
        }


        private void lblcust2_TextChanged(object sender, EventArgs e)
        {
            String selcmd = "SELECT CustomerFirstName, CustomerLastName FROM tblCustomer WHERE CustomerID = '" + lblcust2.Text + "'";
            //Overloading
            populatetext(selcmd, 1);
        }

        private void lblvid2_TextChanged(object sender, EventArgs e)
        {
            String selcmd = "SELECT VehicleName, VehicleBrand FROM tblVehicles WHERE VehicleID = '" + lblvid2.Text + "'";
            //Overloading 
            populatetext(selcmd, 1.5);
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
         
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {

                    PrintDocument pd = new PrintDocument();
                    pd.PrintPage += new PrintPageEventHandler(PrintImage);            
                    pd.Print();

        }

        void PrintImage(object o, PrintPageEventArgs e)
        {
            int x = SystemInformation.WorkingArea.X;
            int y = SystemInformation.WorkingArea.Y;
            int width = this.Width;
            int height = this.Height;

            Rectangle bounds = new Rectangle(x, y, width, height);
            Bitmap image = new Bitmap(width, height);

            this.DrawToBitmap(image, bounds);
            Point p = new Point(100, 100);
            e.Graphics.DrawImage(image, p);


          
        }

        private void btnprints_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {

                Bitmap image2 = new Bitmap(this.Width, this.Height);
                this.DrawToBitmap(image2, new Rectangle(0, 0, this.Width, this.Height));
                image2.Save(dialog.FileName);
                //image2.Save("I:\\invoice.bmp");
            }
        }

        private void btnPrintd_Click(object sender, EventArgs e)
        {
            PrintDialog dialog = new PrintDialog();
            dialog.ShowDialog();
        }


    }
}
