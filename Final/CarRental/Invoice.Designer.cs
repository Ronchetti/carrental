﻿namespace CarRental
{
    partial class Invoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblrent = new System.Windows.Forms.Label();
            this.txtrent = new System.Windows.Forms.TextBox();
            this.lblcustidlb = new System.Windows.Forms.Label();
            this.lblfirst = new System.Windows.Forms.Label();
            this.lbllast = new System.Windows.Forms.Label();
            this.lblrented = new System.Windows.Forms.Label();
            this.lblreturn = new System.Windows.Forms.Label();
            this.lblfee = new System.Windows.Forms.Label();
            this.lbldamage = new System.Windows.Forms.Label();
            this.lblvname = new System.Windows.Forms.Label();
            this.lblvbrand = new System.Windows.Forms.Label();
            this.lblvid = new System.Windows.Forms.Label();
            this.lbltotal = new System.Windows.Forms.Label();
            this.lblcust2 = new System.Windows.Forms.Label();
            this.lblname2 = new System.Windows.Forms.Label();
            this.lbllast2 = new System.Windows.Forms.Label();
            this.lblvname2 = new System.Windows.Forms.Label();
            this.lblbrand2 = new System.Windows.Forms.Label();
            this.lblvid2 = new System.Windows.Forms.Label();
            this.lblrented2 = new System.Windows.Forms.Label();
            this.lblreturned2 = new System.Windows.Forms.Label();
            this.lblfee2 = new System.Windows.Forms.Label();
            this.lbldamage2 = new System.Windows.Forms.Label();
            this.lbltotal2 = new System.Windows.Forms.Label();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnprints = new System.Windows.Forms.Button();
            this.btnPrintd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblrent
            // 
            this.lblrent.AutoSize = true;
            this.lblrent.Location = new System.Drawing.Point(12, 15);
            this.lblrent.Name = "lblrent";
            this.lblrent.Size = new System.Drawing.Size(52, 13);
            this.lblrent.TabIndex = 0;
            this.lblrent.Text = "Rental ID";
            // 
            // txtrent
            // 
            this.txtrent.Location = new System.Drawing.Point(183, 12);
            this.txtrent.Name = "txtrent";
            this.txtrent.Size = new System.Drawing.Size(100, 20);
            this.txtrent.TabIndex = 1;
            this.txtrent.TextChanged += new System.EventHandler(this.txtrent_TextChanged);
            // 
            // lblcustidlb
            // 
            this.lblcustidlb.AutoSize = true;
            this.lblcustidlb.Location = new System.Drawing.Point(13, 45);
            this.lblcustidlb.Name = "lblcustidlb";
            this.lblcustidlb.Size = new System.Drawing.Size(65, 13);
            this.lblcustidlb.TabIndex = 2;
            this.lblcustidlb.Text = "Customer ID";
            // 
            // lblfirst
            // 
            this.lblfirst.AutoSize = true;
            this.lblfirst.Location = new System.Drawing.Point(13, 79);
            this.lblfirst.Name = "lblfirst";
            this.lblfirst.Size = new System.Drawing.Size(55, 13);
            this.lblfirst.TabIndex = 3;
            this.lblfirst.Text = "First name";
            // 
            // lbllast
            // 
            this.lbllast.AutoSize = true;
            this.lbllast.Location = new System.Drawing.Point(13, 106);
            this.lbllast.Name = "lbllast";
            this.lbllast.Size = new System.Drawing.Size(56, 13);
            this.lbllast.TabIndex = 3;
            this.lbllast.Text = "Last name";
            // 
            // lblrented
            // 
            this.lblrented.AutoSize = true;
            this.lblrented.Location = new System.Drawing.Point(13, 224);
            this.lblrented.Name = "lblrented";
            this.lblrented.Size = new System.Drawing.Size(57, 13);
            this.lblrented.TabIndex = 3;
            this.lblrented.Text = "Rented on";
            // 
            // lblreturn
            // 
            this.lblreturn.AutoSize = true;
            this.lblreturn.Location = new System.Drawing.Point(13, 257);
            this.lblreturn.Name = "lblreturn";
            this.lblreturn.Size = new System.Drawing.Size(66, 13);
            this.lblreturn.TabIndex = 3;
            this.lblreturn.Text = "Returned on";
            // 
            // lblfee
            // 
            this.lblfee.AutoSize = true;
            this.lblfee.Location = new System.Drawing.Point(13, 289);
            this.lblfee.Name = "lblfee";
            this.lblfee.Size = new System.Drawing.Size(25, 13);
            this.lblfee.TabIndex = 3;
            this.lblfee.Text = "Fee";
            // 
            // lbldamage
            // 
            this.lbldamage.AutoSize = true;
            this.lbldamage.Location = new System.Drawing.Point(13, 318);
            this.lbldamage.Name = "lbldamage";
            this.lbldamage.Size = new System.Drawing.Size(52, 13);
            this.lbldamage.TabIndex = 3;
            this.lbldamage.Text = "Damages";
            // 
            // lblvname
            // 
            this.lblvname.AutoSize = true;
            this.lblvname.Location = new System.Drawing.Point(13, 136);
            this.lblvname.Name = "lblvname";
            this.lblvname.Size = new System.Drawing.Size(73, 13);
            this.lblvname.TabIndex = 4;
            this.lblvname.Text = "Vehicle Name";
            // 
            // lblvbrand
            // 
            this.lblvbrand.AutoSize = true;
            this.lblvbrand.Location = new System.Drawing.Point(12, 165);
            this.lblvbrand.Name = "lblvbrand";
            this.lblvbrand.Size = new System.Drawing.Size(73, 13);
            this.lblvbrand.TabIndex = 5;
            this.lblvbrand.Text = "Vehicle Brand";
            // 
            // lblvid
            // 
            this.lblvid.AutoSize = true;
            this.lblvid.Location = new System.Drawing.Point(12, 198);
            this.lblvid.Name = "lblvid";
            this.lblvid.Size = new System.Drawing.Size(56, 13);
            this.lblvid.TabIndex = 6;
            this.lblvid.Text = "Vehicle ID";
            // 
            // lbltotal
            // 
            this.lbltotal.AutoSize = true;
            this.lbltotal.Location = new System.Drawing.Point(13, 349);
            this.lbltotal.Name = "lbltotal";
            this.lbltotal.Size = new System.Drawing.Size(31, 13);
            this.lbltotal.TabIndex = 7;
            this.lbltotal.Text = "Total";
            // 
            // lblcust2
            // 
            this.lblcust2.AutoSize = true;
            this.lblcust2.Location = new System.Drawing.Point(183, 45);
            this.lblcust2.Name = "lblcust2";
            this.lblcust2.Size = new System.Drawing.Size(0, 13);
            this.lblcust2.TabIndex = 8;
            this.lblcust2.TextChanged += new System.EventHandler(this.lblcust2_TextChanged);
            this.lblcust2.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblname2
            // 
            this.lblname2.AutoSize = true;
            this.lblname2.Location = new System.Drawing.Point(183, 79);
            this.lblname2.Name = "lblname2";
            this.lblname2.Size = new System.Drawing.Size(0, 13);
            this.lblname2.TabIndex = 8;
            this.lblname2.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbllast2
            // 
            this.lbllast2.AutoSize = true;
            this.lbllast2.Location = new System.Drawing.Point(183, 106);
            this.lbllast2.Name = "lbllast2";
            this.lbllast2.Size = new System.Drawing.Size(0, 13);
            this.lbllast2.TabIndex = 8;
            this.lbllast2.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblvname2
            // 
            this.lblvname2.AutoSize = true;
            this.lblvname2.Location = new System.Drawing.Point(183, 136);
            this.lblvname2.Name = "lblvname2";
            this.lblvname2.Size = new System.Drawing.Size(0, 13);
            this.lblvname2.TabIndex = 8;
            this.lblvname2.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblbrand2
            // 
            this.lblbrand2.AutoSize = true;
            this.lblbrand2.Location = new System.Drawing.Point(183, 165);
            this.lblbrand2.Name = "lblbrand2";
            this.lblbrand2.Size = new System.Drawing.Size(0, 13);
            this.lblbrand2.TabIndex = 8;
            this.lblbrand2.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblvid2
            // 
            this.lblvid2.AutoSize = true;
            this.lblvid2.Location = new System.Drawing.Point(183, 198);
            this.lblvid2.Name = "lblvid2";
            this.lblvid2.Size = new System.Drawing.Size(0, 13);
            this.lblvid2.TabIndex = 8;
            this.lblvid2.TextChanged += new System.EventHandler(this.lblvid2_TextChanged);
            this.lblvid2.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblrented2
            // 
            this.lblrented2.AutoSize = true;
            this.lblrented2.Location = new System.Drawing.Point(183, 224);
            this.lblrented2.Name = "lblrented2";
            this.lblrented2.Size = new System.Drawing.Size(0, 13);
            this.lblrented2.TabIndex = 8;
            this.lblrented2.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblreturned2
            // 
            this.lblreturned2.AutoSize = true;
            this.lblreturned2.Location = new System.Drawing.Point(183, 257);
            this.lblreturned2.Name = "lblreturned2";
            this.lblreturned2.Size = new System.Drawing.Size(0, 13);
            this.lblreturned2.TabIndex = 8;
            this.lblreturned2.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblfee2
            // 
            this.lblfee2.AutoSize = true;
            this.lblfee2.Location = new System.Drawing.Point(183, 289);
            this.lblfee2.Name = "lblfee2";
            this.lblfee2.Size = new System.Drawing.Size(0, 13);
            this.lblfee2.TabIndex = 8;
            this.lblfee2.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbldamage2
            // 
            this.lbldamage2.AutoSize = true;
            this.lbldamage2.Location = new System.Drawing.Point(183, 318);
            this.lbldamage2.Name = "lbldamage2";
            this.lbldamage2.Size = new System.Drawing.Size(0, 13);
            this.lbldamage2.TabIndex = 8;
            this.lbldamage2.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbltotal2
            // 
            this.lbltotal2.AutoSize = true;
            this.lbltotal2.Location = new System.Drawing.Point(183, 349);
            this.lbltotal2.Name = "lbltotal2";
            this.lbltotal2.Size = new System.Drawing.Size(0, 13);
            this.lbltotal2.TabIndex = 8;
            this.lbltotal2.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(197, 410);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(100, 23);
            this.btnPrint.TabIndex = 9;
            this.btnPrint.Text = "Quick print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnprints
            // 
            this.btnprints.Location = new System.Drawing.Point(116, 410);
            this.btnprints.Name = "btnprints";
            this.btnprints.Size = new System.Drawing.Size(75, 23);
            this.btnprints.TabIndex = 10;
            this.btnprints.Text = "Save";
            this.btnprints.UseVisualStyleBackColor = true;
            this.btnprints.Click += new System.EventHandler(this.btnprints_Click);
            // 
            // btnPrintd
            // 
            this.btnPrintd.Location = new System.Drawing.Point(35, 410);
            this.btnPrintd.Name = "btnPrintd";
            this.btnPrintd.Size = new System.Drawing.Size(75, 23);
            this.btnPrintd.TabIndex = 11;
            this.btnPrintd.Text = "Print";
            this.btnPrintd.UseVisualStyleBackColor = true;
            this.btnPrintd.Click += new System.EventHandler(this.btnPrintd_Click);
            // 
            // Invoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 445);
            this.Controls.Add(this.btnPrintd);
            this.Controls.Add(this.btnprints);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.lbltotal2);
            this.Controls.Add(this.lbldamage2);
            this.Controls.Add(this.lblfee2);
            this.Controls.Add(this.lblreturned2);
            this.Controls.Add(this.lblrented2);
            this.Controls.Add(this.lblvid2);
            this.Controls.Add(this.lblbrand2);
            this.Controls.Add(this.lblvname2);
            this.Controls.Add(this.lbllast2);
            this.Controls.Add(this.lblname2);
            this.Controls.Add(this.lblcust2);
            this.Controls.Add(this.lbltotal);
            this.Controls.Add(this.lblvid);
            this.Controls.Add(this.lblvbrand);
            this.Controls.Add(this.lblvname);
            this.Controls.Add(this.lbldamage);
            this.Controls.Add(this.lblfee);
            this.Controls.Add(this.lblreturn);
            this.Controls.Add(this.lblrented);
            this.Controls.Add(this.lbllast);
            this.Controls.Add(this.lblfirst);
            this.Controls.Add(this.lblcustidlb);
            this.Controls.Add(this.txtrent);
            this.Controls.Add(this.lblrent);
            this.Name = "Invoice";
            this.Text = "Invoice";
            this.Load += new System.EventHandler(this.Invoice_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblrent;
        private System.Windows.Forms.TextBox txtrent;
        private System.Windows.Forms.Label lblcustidlb;
        private System.Windows.Forms.Label lblfirst;
        private System.Windows.Forms.Label lbllast;
        private System.Windows.Forms.Label lblrented;
        private System.Windows.Forms.Label lblreturn;
        private System.Windows.Forms.Label lblfee;
        private System.Windows.Forms.Label lbldamage;
        private System.Windows.Forms.Label lblvname;
        private System.Windows.Forms.Label lblvbrand;
        private System.Windows.Forms.Label lblvid;
        private System.Windows.Forms.Label lbltotal;
        private System.Windows.Forms.Label lblcust2;
        private System.Windows.Forms.Label lblname2;
        private System.Windows.Forms.Label lbllast2;
        private System.Windows.Forms.Label lblvname2;
        private System.Windows.Forms.Label lblbrand2;
        private System.Windows.Forms.Label lblvid2;
        private System.Windows.Forms.Label lblrented2;
        private System.Windows.Forms.Label lblreturned2;
        private System.Windows.Forms.Label lblfee2;
        private System.Windows.Forms.Label lbldamage2;
        private System.Windows.Forms.Label lbltotal2;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnprints;
        private System.Windows.Forms.Button btnPrintd;
    }
}