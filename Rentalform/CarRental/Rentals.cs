﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace CarRental
{
    public partial class Rentals : Form
    {
        SqlCeConnection mySqlConnection;
        private string ID;
        public Rentals(string id)
        {
            InitializeComponent();
            txtvehicle.Text = id;
            populatetext();
            populateid();
        }

        public void populatetext()
        {
            int n1, n2, n3, n4, total = 0;
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");


            String selcmd = "SELECT RentalFee, Available, BHP, Size, Modifications, Seats FROM tblVehicles WHERE VehicleID = " + txtvehicle.Text + "";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            //Calculating the days rented
           int day = (dateTimePicker2.Value.DayOfYear - dateTimePicker1.Value.DayOfYear);
           day = day + 1;
           String stringVal = System.Convert.ToString(day);
           lbldays.Text = stringVal;

            try
            {
                mySqlConnection.Open();
                SqlCeDataReader rdr = mySqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    string s4 = rdr["RentalFee"].ToString();
                    txtbase.Text = s4;
                    lblfee.Text = s4;
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //Converting string to integer for total calculation
            n1 = Convert.ToInt32(stringVal);
            n2 = Convert.ToInt32(lblfee.Text);
            n3 = Convert.ToInt32(txtdamage.Text);
            n4 = n1 * n2;
            total = n3 + n4;
            lblcalc.Text = total.ToString();
        }

         public void populateid()
              {
            int rentid = 0;
            string s5;
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            String selcmd = "SELECT MAX(RentalID) AS Expr1 FROM tblRentalInvoice";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();
                SqlCeDataReader rdr = mySqlCommand.ExecuteReader();
                while (rdr.Read())
                {

                   // s5 = rdr["RentalID"].ToString();
                    rentid = rentid + 1;
                    rentid.ToString();
                    //txtbase.Text = s5;
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Rentals_Load(object sender, EventArgs e)
        {

        }

        private void lbldate_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void txtdamage_TextChanged(object sender, EventArgs e)
        {
            populatetext();
            lbldamagefee.Text = txtdamage.Text;
        }

        private void txtrent_TextChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void txtcust_TextChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void txtvehicle_TextChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void txtbase_TextChanged(object sender, EventArgs e)
        {
            populatetext();
        }
    }
}
