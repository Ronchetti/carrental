﻿namespace CarRental
{
    partial class Rentals
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            this.lblrental = new System.Windows.Forms.Label();
            this.lblcustomer = new System.Windows.Forms.Label();
            this.lbldate = new System.Windows.Forms.Label();
            this.lblvehicle = new System.Windows.Forms.Label();
            this.lblbase = new System.Windows.Forms.Label();
            this.lblreturn = new System.Windows.Forms.Label();
            this.lbldamage = new System.Windows.Forms.Label();
            this.lbltotal = new System.Windows.Forms.Label();
            this.txtrent = new System.Windows.Forms.TextBox();
            this.txtcust = new System.Windows.Forms.TextBox();
            this.txtvehicle = new System.Windows.Forms.TextBox();
            this.txtbase = new System.Windows.Forms.TextBox();
            this.txtdamage = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.lbldays = new System.Windows.Forms.Label();
            this.lblfee = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbldamagefee = new System.Windows.Forms.Label();
            this.lblcalc = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblrental
            // 
            this.lblrental.AutoSize = true;
            this.lblrental.Location = new System.Drawing.Point(12, 13);
            this.lblrental.Name = "lblrental";
            this.lblrental.Size = new System.Drawing.Size(49, 13);
            this.lblrental.TabIndex = 0;
            this.lblrental.Text = "RentalID";
            // 
            // lblcustomer
            // 
            this.lblcustomer.AutoSize = true;
            this.lblcustomer.Location = new System.Drawing.Point(12, 44);
            this.lblcustomer.Name = "lblcustomer";
            this.lblcustomer.Size = new System.Drawing.Size(65, 13);
            this.lblcustomer.TabIndex = 1;
            this.lblcustomer.Text = "Customer ID";
            // 
            // lbldate
            // 
            this.lbldate.AutoSize = true;
            this.lbldate.Location = new System.Drawing.Point(193, 13);
            this.lbldate.Name = "lbldate";
            this.lbldate.Size = new System.Drawing.Size(62, 13);
            this.lbldate.TabIndex = 2;
            this.lbldate.Text = "Rental date";
            this.lbldate.Click += new System.EventHandler(this.lbldate_Click);
            // 
            // lblvehicle
            // 
            this.lblvehicle.AutoSize = true;
            this.lblvehicle.Location = new System.Drawing.Point(12, 77);
            this.lblvehicle.Name = "lblvehicle";
            this.lblvehicle.Size = new System.Drawing.Size(56, 13);
            this.lblvehicle.TabIndex = 3;
            this.lblvehicle.Text = "Vehicle ID";
            // 
            // lblbase
            // 
            this.lblbase.AutoSize = true;
            this.lblbase.Location = new System.Drawing.Point(12, 111);
            this.lblbase.Name = "lblbase";
            this.lblbase.Size = new System.Drawing.Size(49, 13);
            this.lblbase.TabIndex = 4;
            this.lblbase.Text = "Base fee";
            // 
            // lblreturn
            // 
            this.lblreturn.AutoSize = true;
            this.lblreturn.Location = new System.Drawing.Point(193, 44);
            this.lblreturn.Name = "lblreturn";
            this.lblreturn.Size = new System.Drawing.Size(63, 13);
            this.lblreturn.TabIndex = 5;
            this.lblreturn.Text = "Return date";
            // 
            // lbldamage
            // 
            this.lbldamage.AutoSize = true;
            this.lbldamage.Location = new System.Drawing.Point(12, 144);
            this.lbldamage.Name = "lbldamage";
            this.lbldamage.Size = new System.Drawing.Size(65, 13);
            this.lbldamage.TabIndex = 6;
            this.lbldamage.Text = "Damage fee";
            // 
            // lbltotal
            // 
            this.lbltotal.AutoSize = true;
            this.lbltotal.Location = new System.Drawing.Point(12, 177);
            this.lbltotal.Name = "lbltotal";
            this.lbltotal.Size = new System.Drawing.Size(31, 13);
            this.lbltotal.TabIndex = 7;
            this.lbltotal.Text = "Total";
            // 
            // txtrent
            // 
            this.txtrent.Location = new System.Drawing.Point(87, 13);
            this.txtrent.Name = "txtrent";
            this.txtrent.Size = new System.Drawing.Size(100, 20);
            this.txtrent.TabIndex = 8;
            this.txtrent.TextChanged += new System.EventHandler(this.txtrent_TextChanged);
            // 
            // txtcust
            // 
            this.txtcust.Location = new System.Drawing.Point(87, 44);
            this.txtcust.Name = "txtcust";
            this.txtcust.Size = new System.Drawing.Size(100, 20);
            this.txtcust.TabIndex = 9;
            this.txtcust.TextChanged += new System.EventHandler(this.txtcust_TextChanged);
            // 
            // txtvehicle
            // 
            this.txtvehicle.Location = new System.Drawing.Point(87, 77);
            this.txtvehicle.Name = "txtvehicle";
            this.txtvehicle.Size = new System.Drawing.Size(100, 20);
            this.txtvehicle.TabIndex = 10;
            this.txtvehicle.TextChanged += new System.EventHandler(this.txtvehicle_TextChanged);
            // 
            // txtbase
            // 
            this.txtbase.Location = new System.Drawing.Point(87, 111);
            this.txtbase.Name = "txtbase";
            this.txtbase.Size = new System.Drawing.Size(100, 20);
            this.txtbase.TabIndex = 11;
            this.txtbase.TextChanged += new System.EventHandler(this.txtbase_TextChanged);
            // 
            // txtdamage
            // 
            this.txtdamage.Location = new System.Drawing.Point(87, 144);
            this.txtdamage.Name = "txtdamage";
            this.txtdamage.Size = new System.Drawing.Size(100, 20);
            this.txtdamage.TabIndex = 12;
            this.txtdamage.Text = "0";
            this.txtdamage.TextChanged += new System.EventHandler(this.txtdamage_TextChanged);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(87, 177);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 13;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(261, 13);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 14;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(261, 44);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 15;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // lbldays
            // 
            this.lbldays.AutoSize = true;
            this.lbldays.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldays.Location = new System.Drawing.Point(395, 78);
            this.lbldays.Name = "lbldays";
            this.lbldays.Size = new System.Drawing.Size(66, 24);
            this.lbldays.TabIndex = 16;
            this.lbldays.Text = "label1";
            // 
            // lblfee
            // 
            this.lblfee.AutoSize = true;
            this.lblfee.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfee.Location = new System.Drawing.Point(395, 106);
            this.lblfee.Name = "lblfee";
            this.lblfee.Size = new System.Drawing.Size(66, 24);
            this.lblfee.TabIndex = 17;
            this.lblfee.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(305, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 24);
            this.label1.TabIndex = 16;
            this.label1.Text = "Days";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(305, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 24);
            this.label2.TabIndex = 17;
            this.label2.Text = "Fee";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(285, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 16);
            this.label3.TabIndex = 18;
            this.label3.Text = "________________________";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(305, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 24);
            this.label4.TabIndex = 17;
            this.label4.Text = "Damage";
            // 
            // lbldamagefee
            // 
            this.lbldamagefee.AutoSize = true;
            this.lbldamagefee.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldamagefee.Location = new System.Drawing.Point(395, 133);
            this.lbldamagefee.Name = "lbldamagefee";
            this.lbldamagefee.Size = new System.Drawing.Size(21, 24);
            this.lbldamagefee.TabIndex = 17;
            this.lbldamagefee.Text = "0";
            // 
            // lblcalc
            // 
            this.lblcalc.AutoSize = true;
            this.lblcalc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcalc.ForeColor = System.Drawing.Color.Red;
            this.lblcalc.Location = new System.Drawing.Point(399, 181);
            this.lblcalc.Name = "lblcalc";
            this.lblcalc.Size = new System.Drawing.Size(51, 20);
            this.lblcalc.TabIndex = 19;
            this.lblcalc.Text = "label5";
            // 
            // Rentals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 310);
            this.Controls.Add(this.lblcalc);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbldamagefee);
            this.Controls.Add(this.lblfee);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbldays);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.txtdamage);
            this.Controls.Add(this.txtbase);
            this.Controls.Add(this.txtvehicle);
            this.Controls.Add(this.txtcust);
            this.Controls.Add(this.txtrent);
            this.Controls.Add(this.lbltotal);
            this.Controls.Add(this.lbldamage);
            this.Controls.Add(this.lblreturn);
            this.Controls.Add(this.lblbase);
            this.Controls.Add(this.lblvehicle);
            this.Controls.Add(this.lbldate);
            this.Controls.Add(this.lblcustomer);
            this.Controls.Add(this.lblrental);
            this.Name = "Rentals";
            this.Text = "Rentals";
            this.Load += new System.EventHandler(this.Rentals_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private System.Windows.Forms.Label lblrental;
        private System.Windows.Forms.Label lblcustomer;
        private System.Windows.Forms.Label lbldate;
        private System.Windows.Forms.Label lblvehicle;
        private System.Windows.Forms.Label lblbase;
        private System.Windows.Forms.Label lblreturn;
        private System.Windows.Forms.Label lbldamage;
        private System.Windows.Forms.Label lbltotal;
        private System.Windows.Forms.TextBox txtrent;
        private System.Windows.Forms.TextBox txtcust;
        private System.Windows.Forms.TextBox txtvehicle;
        private System.Windows.Forms.TextBox txtbase;
        private System.Windows.Forms.TextBox txtdamage;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label lbldays;
        private System.Windows.Forms.Label lblfee;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbldamagefee;
        private System.Windows.Forms.Label lblcalc;
    }
}