﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace CarRental
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnAccounts_Click(object sender, EventArgs e)
        {
            //Instantiating a frmRegister and showing it
            frmRegister Register = new frmRegister();
            Register.Show();
        }

        private void btnVehicles_Click(object sender, EventArgs e)
        {
            //Instantiating a Vehicles form and showing it
            Vehicles Vehicles = new Vehicles();
            Vehicles.Show();
        }

        private void btnbrowse_Click(object sender, EventArgs e)
        {
            //Instantiating a Browser form and showing it
            Browser browser = new Browser();
            browser.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Modified constructor for Rentals requires a string to passed over
            String s = "1";
            //Instantiating a Rentals form and showing it        
            Rentals rental = new Rentals(s);
            rental.Show();
        }

        private void btnrentlist_Click(object sender, EventArgs e)
        {
            //Instantiating a RentList form  and showing it
            RentList rental = new RentList();
            rental.Show();
        }
    }
}
