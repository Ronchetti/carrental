﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace CarRental
{
    public partial class Rentals : Form
    {
        SqlCeConnection mySqlConnection;
        private string ID;
        //string passed in via constructor
        public Rentals(string id)
        {
            InitializeComponent();
            //Utilising the string passed in, and inputting it into a text box
            txtvehicle.Text = id;
            populatetext();
        }

        public void populatetext()
        {
            //Initialising variables
            int n1, n2, n3, n4, total = 0;

            //Connecting to database
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");

            //SQL statement and executing it - using the value passed in 
            String selcmd = "SELECT RentalFee, Available, BHP, Size, Modifications, Seats FROM tblVehicles WHERE VehicleID = " + txtvehicle.Text + "";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            //Calculating the days rented
           int day = (dateTimePicker2.Value.DayOfYear - dateTimePicker1.Value.DayOfYear);
           day = day + 1;

           //Creating new string and assigning it to the number of days
           String stringVal = System.Convert.ToString(day);

            //Assigning total number of days to a label
           lbldays.Text = stringVal;
            try
            {
                //Opening a new connection and a reader
                mySqlConnection.Open();
                SqlCeDataReader rdr = mySqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    //Assigning the fee to a string
                    string s4 = rdr["RentalFee"].ToString();
                    //Assigning the string to a text box and a label
                    txtbase.Text = s4;
                    lblfee.Text = s4;
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            //Converting string to integer for total calculation
            n1 = Convert.ToInt32(stringVal);
            n2 = Convert.ToInt32(lblfee.Text);
            n3 = Convert.ToInt32(txtdamage.Text);
            //Calculating the total rental price
            n4 = n1 * n2;
            total = n3 + n4;
            //Assigning total to a label
            lblcalc.Text = total.ToString();
        }

      
         public bool checkInputs()
         {
             bool rtnvalue = true;

             if (
                 //Checks to make sure there is value's in the main fields
                 string.IsNullOrEmpty(txtcust.Text) ||
                 string.IsNullOrEmpty(txtvehicle.Text) ||
                 string.IsNullOrEmpty(txtbase.Text) ||
                 string.IsNullOrEmpty(txtdamage.Text))
             {
                 //display a message box with a error message
                 MessageBox.Show("Error: Please check your inputs");
                 rtnvalue = false;
             }

             return (rtnvalue);
         }

         public void insertRecord(String name, String brand, String type, String fee, String available, String bhp, String size, String commandString)
         {
             try
             {
                 //SQL command and connection passed in via constructor
                 SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                 //Parameterising query values 
                 cmdInsert.Parameters.AddWithValue("@cust", name);
                 cmdInsert.Parameters.AddWithValue("@vehi", brand);
                 cmdInsert.Parameters.AddWithValue("@rentd", type);
                 cmdInsert.Parameters.AddWithValue("@retu", fee);
                 cmdInsert.Parameters.AddWithValue("@fee", available);
                 cmdInsert.Parameters.AddWithValue("@tota", bhp);
                 cmdInsert.Parameters.AddWithValue("@dama", size);
                 cmdInsert.ExecuteNonQuery();
             }
             catch (SqlCeException ex)
             {
                 MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
             }

         }

         public void updaterecord(String name, String commandString)
         {

             try
             {
                 //SQL command and connection passed in via constructor
                 SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);
                 //Parameterising query values
                 cmdInsert.Parameters.AddWithValue("@vehi", name);
                 cmdInsert.ExecuteNonQuery();
             }
             catch (SqlCeException ex)
             {
                 MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
             }

         }


         public void cleartxtBoxes()
         {
             //Reseting text boxes
            txtcust.Text = txtvehicle.Text = txtbase.Text = "";
             txtdamage.Text = "0";
         }

        private void Rentals_Load(object sender, EventArgs e)
        {

        }

        private void lbldate_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void txtdamage_TextChanged(object sender, EventArgs e)
        {
            populatetext();
            lbldamagefee.Text = txtdamage.Text;
        }

        private void txtrent_TextChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void txtcust_TextChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void txtvehicle_TextChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void txtbase_TextChanged(object sender, EventArgs e)
        {
            populatetext();
        }

        private void btnRent_Click(object sender, EventArgs e)
        {
            //Creates to string variables and sets their value
            string d1, d2 = "";
            //SQL statements to executed when renting a vehicle
            //One statement for inserting a new rental invoice into the table
            //Other SQL statement for making the rented vehicle unavailable 
            String commandString = "INSERT INTO tblRentalInvoice(CustomerID, VehicleID, RentalDate, ReturnDate, Fee, Total, Damage) VALUES (@cust, @vehi, @rentd, @retu, @fee, @tota, @dama)";
            String updatestring = "UPDATE tblVehicles SET Available= 'False' WHERE VehicleID = @vehi";
            //Converts the selected dates to string
            d1 = dateTimePicker1.Value.ToShortDateString();
            d2 = dateTimePicker2.Value.ToShortDateString();

            //Calling method and passing across appropiate variables for the constructors
           insertRecord(txtcust.Text, txtvehicle.Text, d1, d2, txtbase.Text, lblcalc.Text, txtdamage.Text, commandString);
           updaterecord(txtvehicle.Text, updatestring);

            //Opens a new form and closes the current
           RentList df = new RentList();
           df.Show();
           this.Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
