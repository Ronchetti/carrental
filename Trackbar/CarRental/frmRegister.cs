﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace CarRental
{
    public partial class frmRegister : Form
    {
        SqlCeConnection mySqlConnection;
        private string ID;
        public frmRegister()
        {
            InitializeComponent();
            //Populating list box on form load
            populateListBox();
        }


        public void populateListBox()
        {
            //Connecting to db
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            //SQL statement to get data for list box
            String selcmd = "SELECT CustomerID, CustomerFirstName, CustomerLastName, CustomerPhone, CustomerEmail FROM tblcustomer ORDER BY customerID";
            //Executing the listbox
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
            try
            {
                //Opening a new connecting and creating a reader
                mySqlConnection.Open();
                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                //Clearing data before adding new
                lbxstudents.Items.Clear();
                while (mySqlDataReader.Read())
                {
                    //Adding customer information to the list box
                    lbxstudents.Items.Add(mySqlDataReader["CustomerID"] + " " +
                         mySqlDataReader["CustomerFirstName"] + " " + mySqlDataReader["CustomerLastName"] + " " + mySqlDataReader["CustomerPhone"] + " " + mySqlDataReader["CustomerEmail"]);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void populatetext()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");

            String selcmd = "SELECT CustomerID, CustomerFirstName, CustomerLastName, CustomerPhone, CustomerEmail FROM tblcustomer WHERE CustomerID = " + txtID.Text + "";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader rdr = mySqlCommand.ExecuteReader();
                while (rdr.Read())
                {
                    string s1 = rdr["CustomerFirstName"].ToString();
                    txtfirst.Text = s1;
                    string s2 = rdr["CustomerLastName"].ToString();
                    txtlast.Text = s2;
                    string s3 = rdr["CustomerPhone"].ToString();
                    txtphone.Text = s3;
                    string s4 = rdr["CustomerEmail"].ToString();
                    txtemail.Text = s4;
                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        public void cleartxtBoxes()
        {
            txtID.Text = txtfirst.Text = txtlast.Text = txtphone.Text = txtemail.Text = "";
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtID.Text) ||
                string.IsNullOrEmpty(txtfirst.Text) ||
                string.IsNullOrEmpty(txtlast.Text) ||
                string.IsNullOrEmpty(txtphone.Text) ||
                string.IsNullOrEmpty(txtemail.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }
        public bool checkInputsDelete()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtID.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }


        public void insertRecord(String ID, String first, String last, String phone, String email, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@ID", ID);
                cmdInsert.Parameters.AddWithValue("@first", first);
                cmdInsert.Parameters.AddWithValue("@last", last);
                cmdInsert.Parameters.AddWithValue("@phone", phone);
                cmdInsert.Parameters.AddWithValue("@email", email);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void deleteRecord(String ID, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@ID", ID);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void frmRegister_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            if (checkInputs())
            {

                String commandString = "INSERT INTO tblcustomer(CustomerID, CustomerFirstName, CustomerLastName, CustomerPhone, CustomerEmail) VALUES (@ID, @first, @last, @phone, @email)";

                insertRecord(txtID.Text, txtfirst.Text, txtlast.Text, txtphone.Text, txtemail.Text, commandString);
                populateListBox();
                cleartxtBoxes();
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (checkInputsDelete())
            {

                String commandString = "DELETE FROM tblcustomer WHERE CustomerID = @ID";

                deleteRecord(txtID.Text, commandString);
                populateListBox();
                cleartxtBoxes();
            }
        }

        private void lbxstudents_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {

                String commandString = "UPDATE tblCustomer SET CustomerID=@ID, CustomerFirstName=@first, CustomerLastName=@last, CustomerPhone=@phone, CustomerEmail=@email WHERE CustomerID = @ID";

                //Executes SQL statement with specified parameters
                insertRecord(txtID.Text, txtfirst.Text, txtlast.Text, txtphone.Text, txtemail.Text, commandString);
                populateListBox();
                cleartxtBoxes();
            }
        }

        private void btnpull_Click(object sender, EventArgs e)
        {
            if (checkInputsDelete())
            {
                String commandString = "SELECT CustomerID, CustomerFirstName, CustomerLastName, CustomerPhone, CustomerEmail FROM tblcustomer WHERE CustomerID = " + txtID.Text + "";
                //Executes SQL statement with specified parameters
                //deleteRecord(txtID.Text, commandString);

                populatetext();
                //Use this button to add values into the text fields 
            }
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            cleartxtBoxes();

        }
        }
    }
