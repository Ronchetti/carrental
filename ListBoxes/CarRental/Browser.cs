﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace CarRental
{
    public partial class Browser : Form
    {
        SqlCeConnection mySqlConnection;
        private string ID;
        public Browser()
        {
            InitializeComponent();
            populatetype();
            //populateseats();
        }

        public void populatetype()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            String selcmd = "SELECT DISTINCT VehicleType FROM tblVehicles  WHERE Available = 'True' ORDER BY VehicleType";
            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                lbxtype.Items.Clear();
                while (mySqlDataReader.Read())
                {
                    lbxtype.Items.Add(mySqlDataReader["VehicleType"]);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void populateseats(string command)
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            SqlCeCommand mySqlCommand = new SqlCeCommand(command, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                lbxseats.Items.Clear();
                while (mySqlDataReader.Read())
                {
                    lbxseats.Items.Add(mySqlDataReader["Seats"]);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void populatebrand(string command)
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");
            SqlCeCommand mySqlCommand = new SqlCeCommand(command, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                lbxbrand.Items.Clear();
                while (mySqlDataReader.Read())
                {
                    lbxbrand.Items.Add(mySqlDataReader["VehicleBrand"]);
                }
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Browser_Load(object sender, EventArgs e)
        {

        }

        private void lblseats_Click(object sender, EventArgs e)
        {

        }

        private void lbxtype_SelectedIndexChanged(object sender, EventArgs e)
        {
           // string selected = lbxtype.SelectedItem.ToString();            
            String commandString = "SELECT DISTINCT Seats FROM tblVehicles WHERE VehicleType = '"+ lbxtype.SelectedItem.ToString() +"' AND Available = 'True'";
            populateseats(commandString);
            lbxbrand.Items.Clear();
        }

        private void lbxseats_SelectedIndexChanged(object sender, EventArgs e)
        {
           // string selected = lbxseats.SelectedItem.ToString();
            String commandString = "SELECT DISTINCT VehicleBrand FROM tblVehicles WHERE (VehicleType = '" + lbxtype.SelectedItem.ToString() + "') AND (Available = 'True') AND (Seats = '"+ lbxseats.SelectedItem.ToString() +"')";
            populatebrand(commandString);
        }
    }
}
