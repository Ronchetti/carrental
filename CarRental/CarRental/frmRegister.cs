﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace CarRental
{
    public partial class frmRegister : Form
    {
        SqlCeConnection mySqlConnection;
        private string ID;
        public frmRegister()
        {
            InitializeComponent();
            populateListBox();

        }


        public void populateListBox()
        {
            mySqlConnection =
                    new SqlCeConnection(@"Data Source=I:\ASEa\CarRental\Mydatabase.sdf ");

            String selcmd = "SELECT CustomerID, CustomerFirstName, CustomerLastName, CustomerPhone, CustomerEmail FROM tblcustomer ORDER BY customerID";

            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                lbxstudents.Items.Clear();

                while (mySqlDataReader.Read())
                {

                    lbxstudents.Items.Add(mySqlDataReader["CustomerID"] + " " +
                         mySqlDataReader["CustomerFirstName"] + " " + mySqlDataReader["CustomerLastName"] + " " + mySqlDataReader["CustomerPhone"] + " " + mySqlDataReader["CustomerEmail"]);


                }
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        public void cleartxtBoxes()
        {
            txtID.Text = txtfirst.Text = txtlast.Text = txtphone.Text = txtemail.Text = "";
        }

        public bool checkInputs()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtID.Text) ||
                string.IsNullOrEmpty(txtfirst.Text) ||
                string.IsNullOrEmpty(txtlast.Text) ||
                string.IsNullOrEmpty(txtphone.Text) ||
                string.IsNullOrEmpty(txtemail.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }
        public bool checkInputsDelete()
        {
            bool rtnvalue = true;

            if (string.IsNullOrEmpty(txtID.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }


        public void insertRecord(String ID, String first, String last, String phone, String email, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@ID", ID);
                cmdInsert.Parameters.AddWithValue("@first", first);
                cmdInsert.Parameters.AddWithValue("@last", last);
                cmdInsert.Parameters.AddWithValue("@phone", phone);
                cmdInsert.Parameters.AddWithValue("@email", email);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void deleteRecord(String ID, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                cmdInsert.Parameters.AddWithValue("@ID", ID);
                cmdInsert.ExecuteNonQuery();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ID + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void frmRegister_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            if (checkInputs())
            {

                String commandString = "INSERT INTO tblcustomer(CustomerID, CustomerFirstName, CustomerLastName, CustomerPhone, CustomerEmail) VALUES (@ID, @first, @last, @phone, @email)";

                insertRecord(txtID.Text, txtfirst.Text, txtlast.Text, txtphone.Text, txtemail.Text, commandString);
                populateListBox();
                cleartxtBoxes();
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (checkInputsDelete())
            {

                String commandString = "DELETE FROM tblcustomer WHERE CustomerID = @ID";

                deleteRecord(txtID.Text, commandString);
                populateListBox();
                cleartxtBoxes();
            }
        }

        private void lbxstudents_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
